package pl.sdacademy.databasedao;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.greenrobot.greendao.database.Database;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.sdacademy.databasedao.db.DaoMaster;
import pl.sdacademy.databasedao.db.DaoSession;
import pl.sdacademy.databasedao.db.Produkt;
import pl.sdacademy.databasedao.db.ProduktDao;

public class MainActivity extends AppCompatActivity {

    private Long id;
    private String nazwa;
    private Float cena;

    private DaoSession daoSession;
    ProduktDao produktDao;

    RecyclerView myRecycler;
    LinearLayoutManager llm;
    RestauracjaAdapter adapter;

    List<Produkt> produkty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        DaoMaster.DevOpenHelper pomocnik = new DaoMaster.DevOpenHelper(MainActivity.this, "users.db");
        Database db = pomocnik.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
        produktDao = daoSession.getProduktDao();

        produkty = produktDao.queryBuilder().list();
        myRecycler = (RecyclerView) findViewById(R.id.myRecycleView);
        llm = new LinearLayoutManager(MainActivity.this);
        myRecycler.setLayoutManager(llm);
        adapter = new RestauracjaAdapter(produkty, MainActivity.this);
        myRecycler.setAdapter(adapter);

    }

    @OnClick(R.id.faby)
    public void add() {
        final Context context = MainActivity.this;

        Toast.makeText(context, "TEST", Toast.LENGTH_SHORT).show();

        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText dialogTitle = new EditText(context);
        dialogTitle.setHint("Rodzaj");
        layout.addView(dialogTitle);

        final EditText dialogMoney = new EditText(context);
        dialogMoney.setHint("Cena");
        layout.addView(dialogMoney);


        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setView(layout);
        alertDialog.setTitle("Wpisz produkt")
                .setPositiveButton("Dodaj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        produktDao = daoSession.getProduktDao();

                        Produkt pr = new Produkt();
                        pr.setNazwa(dialogTitle.getText().toString());
                        pr.setCena(Float.valueOf(dialogMoney.getText().toString()));

                        produktDao.insert(pr);

                        odczytajDaneZBazy();
                        myRecycler.setAdapter(new RestauracjaAdapter(produkty, MainActivity.this));

                    }
                })
                .setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).
                setCancelable(true).show();


    }

    public void odczytajDaneZBazy(){
        produkty = produktDao.queryBuilder().list();
    }
}
