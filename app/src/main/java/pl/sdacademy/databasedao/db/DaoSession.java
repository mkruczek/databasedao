package pl.sdacademy.databasedao.db;

import java.util.Map;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

import pl.sdacademy.databasedao.db.Produkt;
import pl.sdacademy.databasedao.db.Koszyk;
import pl.sdacademy.databasedao.db.Zamowienia;
import pl.sdacademy.databasedao.db.szczegoly_zamowienia;

import pl.sdacademy.databasedao.db.ProduktDao;
import pl.sdacademy.databasedao.db.KoszykDao;
import pl.sdacademy.databasedao.db.ZamowieniaDao;
import pl.sdacademy.databasedao.db.szczegoly_zamowieniaDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see org.greenrobot.greendao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig produktDaoConfig;
    private final DaoConfig koszykDaoConfig;
    private final DaoConfig zamowieniaDaoConfig;
    private final DaoConfig szczegoly_zamowieniaDaoConfig;

    private final ProduktDao produktDao;
    private final KoszykDao koszykDao;
    private final ZamowieniaDao zamowieniaDao;
    private final szczegoly_zamowieniaDao szczegoly_zamowieniaDao;

    public DaoSession(Database db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        produktDaoConfig = daoConfigMap.get(ProduktDao.class).clone();
        produktDaoConfig.initIdentityScope(type);

        koszykDaoConfig = daoConfigMap.get(KoszykDao.class).clone();
        koszykDaoConfig.initIdentityScope(type);

        zamowieniaDaoConfig = daoConfigMap.get(ZamowieniaDao.class).clone();
        zamowieniaDaoConfig.initIdentityScope(type);

        szczegoly_zamowieniaDaoConfig = daoConfigMap.get(szczegoly_zamowieniaDao.class).clone();
        szczegoly_zamowieniaDaoConfig.initIdentityScope(type);

        produktDao = new ProduktDao(produktDaoConfig, this);
        koszykDao = new KoszykDao(koszykDaoConfig, this);
        zamowieniaDao = new ZamowieniaDao(zamowieniaDaoConfig, this);
        szczegoly_zamowieniaDao = new szczegoly_zamowieniaDao(szczegoly_zamowieniaDaoConfig, this);

        registerDao(Produkt.class, produktDao);
        registerDao(Koszyk.class, koszykDao);
        registerDao(Zamowienia.class, zamowieniaDao);
        registerDao(szczegoly_zamowienia.class, szczegoly_zamowieniaDao);
    }
    
    public void clear() {
        produktDaoConfig.clearIdentityScope();
        koszykDaoConfig.clearIdentityScope();
        zamowieniaDaoConfig.clearIdentityScope();
        szczegoly_zamowieniaDaoConfig.clearIdentityScope();
    }

    public ProduktDao getProduktDao() {
        return produktDao;
    }

    public KoszykDao getKoszykDao() {
        return koszykDao;
    }

    public ZamowieniaDao getZamowieniaDao() {
        return zamowieniaDao;
    }

    public szczegoly_zamowieniaDao getSzczegoly_zamowieniaDao() {
        return szczegoly_zamowieniaDao;
    }

}
