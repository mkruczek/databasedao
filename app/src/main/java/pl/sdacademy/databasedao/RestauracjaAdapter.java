package pl.sdacademy.databasedao;

import android.content.ContentValues;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.greenrobot.greendao.database.Database;

import java.util.List;

import pl.sdacademy.databasedao.db.DaoMaster;
import pl.sdacademy.databasedao.db.DaoSession;
import pl.sdacademy.databasedao.db.Koszyk;
import pl.sdacademy.databasedao.db.KoszykDao;
import pl.sdacademy.databasedao.db.Produkt;
import pl.sdacademy.databasedao.db.ProduktDao;

/**
 * Created by RENT on 2017-07-07.
 */

public class RestauracjaAdapter extends RecyclerView.Adapter<RestauracjaAdapter.MyViewHolder> {

    private List<Produkt> produkty;
    private Context context;

    public RestauracjaAdapter(List<Produkt> produkty, Context context) {
        this.produkty = produkty;
        this.context = context;
    }

    @Override
    public RestauracjaAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.one_row, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RestauracjaAdapter.MyViewHolder holder, final int position) {

        final Produkt produkt = produkty.get(position);
        DaoMaster.DevOpenHelper pomocnik = new DaoMaster.DevOpenHelper(context, "users.db");
        Database db = pomocnik.getWritableDb();
        DaoSession daoSession = new DaoMaster(db).newSession();
        final KoszykDao koszykDao = daoSession.getKoszykDao();

        final List<Koszyk> listaKosz = koszykDao.queryBuilder().where(KoszykDao.Properties.ProduktId.eq(produkt.getId())).list();

        holder.editTextProdukt.setText(produkt.getNazwa());
        holder.editTextCena.setText(String.valueOf(produkt.getCena()));
        if(listaKosz.size() == 0){

        holder.editTextIlosc.setText("0");
        }else {
            Koszyk koszykDoAktualizacji = listaKosz.get(0);
        holder.editTextIlosc.setText(koszykDoAktualizacji.getIlosc().toString());
        }
        holder.dodajButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (listaKosz.size() == 0) {
                    Koszyk koszyk = new Koszyk();
                    koszyk.setIlosc(1);
                    koszyk.setProduktId(produkt.getId());
                    koszykDao.insert(koszyk);
                } else {
                    Koszyk koszykDoAktualizacji = listaKosz.get(0);
                    koszykDoAktualizacji.setIlosc(koszykDoAktualizacji.getIlosc() + 1);
                    koszykDao.update(koszykDoAktualizacji);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return produkty.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView editTextCena;
        TextView editTextProdukt;
        Button dodajButton;
        TextView editTextIlosc;


        public MyViewHolder(View itemView) {
            super(itemView);
            editTextProdukt = (TextView) itemView.findViewById(R.id.editTextProdukt);
            editTextCena = (TextView) itemView.findViewById(R.id.editTextCena);
            dodajButton = (Button) itemView.findViewById(R.id.buttonDodaj);
            editTextIlosc = (TextView) itemView.findViewById(R.id.editTextIlosc);
        }
    }
}
