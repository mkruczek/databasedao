package com.greendao;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Property;
import org.greenrobot.greendao.generator.Schema;

public class MyGenerator {

    public static void main(String[] args) {
        Schema schema = new Schema(3, "pl.sdacademy.databasedao.db");
        schema.enableKeepSectionsByDefault();

        addTables(schema);


        try {
            new DaoGenerator().generateAll(schema, "./app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private static void addTables(Schema schema) {
//        addProduktyEntities(schema);
//        addZamowieniaEntity(schema);


        Entity produkt = schema.addEntity("Produkt");
        produkt.addIdProperty().primaryKey().autoincrement();
        produkt.addStringProperty("nazwa");
        produkt.addFloatProperty("cena");


        Entity koszyk = schema.addEntity("Koszyk");
        koszyk.addIdProperty().primaryKey().autoincrement();
        Property produktIdProperty = koszyk.addLongProperty("produktId").getProperty();
        koszyk.addToOne(produkt, produktIdProperty);
        koszyk.addIntProperty("ilosc");


        Entity zamowienia = schema.addEntity("Zamowienia");
        zamowienia.addIdProperty().primaryKey().autoincrement();
        zamowienia.addFloatProperty("suma");
        zamowienia.addDateProperty("data");
        zamowienia.addStringProperty("imie");
        zamowienia.addStringProperty("nazwisko");
        zamowienia.addStringProperty("ulica");
        zamowienia.addStringProperty("kod_pocztowy");
        zamowienia.addStringProperty("miasto");


        Entity szczegolyZamowienia = schema.addEntity("szczegoly_zamowienia");
        szczegolyZamowienia.addIntProperty("ilosc");
        szczegolyZamowienia.addIdProperty().primaryKey().autoincrement();
        Property produktIdPropertyDlaSzczegoly = szczegolyZamowienia.addLongProperty("produkId").getProperty();
        szczegolyZamowienia.addToOne(produkt, produktIdPropertyDlaSzczegoly);
        Property zamowieniaIdPropertyDlaSzczegoly = szczegolyZamowienia.addLongProperty("zamowienieId").getProperty();
        szczegolyZamowienia.addToOne(zamowienia, zamowieniaIdPropertyDlaSzczegoly);

    }

}
